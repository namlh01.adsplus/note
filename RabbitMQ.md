# **RabbitMQ**

## **RabbitMQ là gì ?**
**RabbitMQ** là một **message broker** mã nguồn mở dựa trên giao thức **AMQP** để truyền tải các thông điệp giữa các ứng dụng khác nhau. Nó hoạt động dựa trên kiến trúc hàng đợi, nơi các ứng dụng gửi các thông điệp đến một hàng đợi, và các ứng dụng khác lấy các thông điệp từ hàng đợi để xử lý. **RabbitMQ** được sử dụng rộng rãi trong các hệ thống phân tán, **microservices** và các ứng dụng IoT, với khả năng xử lý hàng triệu thông điệp mỗi giây và cung cấp các tính năng như đảm bảo tính toàn vẹn dữ liệu, khả năng mở rộng dễ dàng và khả năng tích hợp với các công nghệ khác. 

## **Các điểm mạnh về RabbitMQ**
* **Khả năng xử lý và phân phối thông điệp :** **RabbitMQ** hỗ trợ nhiều giao thức và cơ chế phân phối thông điệp như **point-to-point**, **publish/subscribe**, **routing**, **topic** **exchange**, **direct exchange**, **etc**. Điều này cho phép các ứng dụng gửi và nhận các thông điệp một cách dễ dàng và đáng tin cậy.

* **Hỗ trợ nhiều giao thức :** **RabbitMQ** hỗ trợ các giao thức truyền tải thông điệp như **AMQP** (Advanced Message Queuing Protocol), **MQTT** (Message Queuing Telemetry Transport), **STOMP** (Streaming Text Oriented Messaging Protocol) và **HTTP**.

* **Tin nhắn đảm bảo được xử lý :**  **RabbitMQ** sử dụng hệ thống xếp hàng (**queue**) để lưu trữ các tin nhắn. Khi tin nhắn được gửi đến, **RabbitMQ** sẽ đảm bảo rằng tin nhắn được gửi đến đúng hàng đợi và được xử lý đúng thứ tự.
* **Độ tin cậy cao :** **RabbitMQ** sử dụng các cơ chế bảo mật và đảm bảo tính toàn vẹn của thông điệp, đảm bảo rằng thông điệp sẽ được gửi và nhận một cách đáng tin cậy.

* **Linh hoạt và mở rộng :** **RabbitMQ** được thiết kế để dễ dàng mở rộng, cho phép các ứng dụng linh hoạt thay đổi kích thước và cấu hình của hệ thống mà không gây ra sự gián đoạn trong hoạt động.

* **Tích hơp tốt :** **RabbitMQ** tích hợp tốt với các ngôn ngữ lập trình phổ biến như `Java` , `Python` , `Ruby` , `.NET` , `PHP` , `Node.js` , `Go` , `C#` , `Objective-C` , `Swift`, và nhiều hơn nữa.

* **Hỗ trợ đa nền tảng :** **RabbitMQ** có thể chạy trên nhiều nền tảng khác nhau như **Linux**, **Windows**, **macOS**, **Solaris**, **FreeBSD**, **AIX**, và nhiều hơn nữa.

## **Các điểm yếu về RabbitMQ**
* **Sự phức tạp :** **RabbitMQ** là một hệ thống phân phối tin nhắn đa dạng và mạnh mẽ, nhưng cũng khá phức tạp và đòi hỏi nhiều kiến thức kỹ thuật để triển khai, cấu hình và quản lý.

* **Hiệu suất thấp:** Một số vấn đề về hiệu suất có thể xảy ra khi sử dụng các cấu hình và kiến trúc không phù hợp. Ví dụ như **queue** nhận được quá nhiều tin nhắn cùng lúc có thể gây chậm trễ hoặc ngưng hoạt động.

* **Cấu hình phức tạp:** Để tận dụng tối đa các tính năng và khả năng của **RabbitMQ**, cấu hình các thông số quan trọng như **exchange**, **queue**, **binding**, v.v. đòi hỏi sự hiểu biết về các khái niệm liên quan đến **RabbitMQ** và kinh nghiệm trong việc cấu hình.

## **Cách sử dụng RabbitMQ ?**
Để sử dụng **RabbitMQ**, ta cần cài đặt **RabbitMQ** trên máy tính của mình hoặc trên một máy chủ. Sau đó, ta có thể sử dụng các thư viện **API** để tạo và quản lý các queue, gửi và nhận các thông điệp từ các **queue**, và xử lý các thông điệp đó theo cách mà mình muốn.
* **Cài đặt RabbitMQ :** ta cần cài đặt **RabbitMQ** trên máy tính của mình hoặc trên một máy chủ. **RabbitMQ** có thể được cài đặt trên các hệ điều hành khác nhau như **Windows**, **Linux** hoặc **macOS**. Để cài đặt **RabbitMQ**, ta có thể tải xuống phiên bản phù hợp với hệ điều hành của mình từ trang web chính thức của **RabbitMQ**.

* **Tạo các queues :** Sau khi cài đặt **RabbitMQ**, ta có thể sử dụng giao diện quản trị web hoặc các thư viện **API** để tạo các **queues**. Một queue trong **RabbitMQ** là một vùng nhớ được sử dụng để lưu trữ các tin nhắn.

* **Gửi tin nhắn vào queue :** Sau khi tạo các **queues**, ta có thể sử dụng các thư viện **API** để gửi các tin nhắn vào queue. Các tin nhắn này có thể được gửi bằng các ngôn ngữ lập trình khác nhau như `Java`, `Python`, `Ruby`, `.NET`, `PHP`.
* **Nhận tin nhắn từ queue :** Nhận tin nhắn từ **queue** Sau khi tin nhắn được gửi vào **queue**, ta có thể sử dụng các thư viện **API** để nhận các tin nhắn này từ queue. Các tin nhắn này có thể được xử lý hoặc chuyển tiếp đến các **queue** khác.v
* **Xử lý tin nhắn :** Khi tin nhắn được nhận từ **queue**, ta có thể xử lý nó bằng các thao tác phù hợp với nhu cầu của hệ thống. Các thao tác này có thể bao gồm kiểm tra tính hợp lệ của tin nhắn, lưu trữ dữ liệu vào cơ sở dữ liệu hoặc gửi các tin nhắn khác đến các **queue** khác.
* **Xóa queue :** Khi không còn cần sử dụng **queue** nữa, ta có thể sử dụng giao diện quản trị web hoặc các thư viện **API** để xóa **queue** đó.


<center>

<span style="margin:center;">![ Sơ đồ RabbitMQ ](https://gpcoder.com/wp-content/uploads/2020/06/RabbitMQ-DirectExchange.png)</span>

</center>


## **Ứng dụng của RabbitMQ**
* **Xử lý thông tin trong các ứng dụng web :** **RabbitMQ** có thể được sử dụng để xử lý các yêu cầu của khách hàng và các sự kiện trên các trang web.

* **Hệ thống phân tán :** **RabbitMQ** được sử dụng để trao đổi thông tin giữa các ứng dụng trong các hệ thống phân tán.

* **IoT (Internet of Things) :** **RabbitMQ** có thể được sử dụng để xử lý các thông điệp trong các ứng dụng **IoT**.

* **Xử lý dữ liệu lớn :** **RabbitMQ** có thể được sử dụng để xử lý các thông điệp trong các ứng dụng **Big Data**.

* **Ứng dụng điện toán đám mây :** **RabbitMQ** có thể được sử dụng để trao đổi thông tin giữa các ứng dụng trong các môi trường điện toán đám mây.
 
## **Demo RabbitMQ**

* Tải xuống bản cài đặt **RabbitMQ** tương ứng với hệ điều hành của bạn từ [trang chủ](https://www.rabbitmq.com/download.html) của **RabbitMQ** . 

* **Cài đặt Erlang :** **RabbitMQ** chạy trên **Erlang**, vì vậy cần phải cài đặt **Erlang** trước khi cài đặt **RabbitMQ**. Bạn có thể tải phiên bản mới nhất của **Erlang** từ trang web chính thức của **Erlang** và cài đặt nó theo hướng dẫn trên trang web.

* Sau khi cài đặt xong, kiểm tra trạng thái của **RabbitMQ** bằng cách mở trình duyệt web và truy cập vào địa chỉ http://localhost:15672 hoặc kiểm tra câu lệnh **rabbitmq-server**. Nếu thành công, bạn sẽ thấy trang quản trị **RabbitMQ**.

### Những câu lệnh được sử dụng trong **Terminal RabbitMQ** 
<hr>

* `rabbitmq-server` - Khởi động server RabbitMQ.
* `rabbitmqctl status` - Kiểm tra trạng thái của RabbitMQ server.
* `rabbitmqctl start_app` - Khởi động ứng dụng RabbitMQ.
* `rabbitmqctl stop_app` - Dừng ứng dụng RabbitMQ.
* `rabbitmqctl reset` - Xóa toàn bộ dữ liệu và khởi tạo lại RabbitMQ.
* `rabbitmqctl list_queues` - Hiển thị danh sách các hàng đợi (queues) hiện có.
* `rabbitmqctl list_exchanges` - Hiển thị danh sách các exchange hiện có.
* `rabbitmqctl list_bindings` - Hiển thị danh sách các kết nối (bindings) hiện có.
* `rabbitmqadmin declare queue name=queue_name` - Tạo một hàng đợi mới với tên là `queue_name`.
* `rabbitmqadmin publish exchange=exchange_name routing_key=routing_key payload="message"` - Gửi một thông điệp đến exchange exchange_name với routing key routing_key.
* `rabbitmqadmin get queue=queue_name` - Lấy thông điệp từ hàng đợi có tên là `queue_name`.
* `rabbitmqadmin declare exchange name=exchange_name type=exchange_type` - Tạo một **exchange** mới với tên là `exchange_name` và kiểu là `exchange_type`. 


